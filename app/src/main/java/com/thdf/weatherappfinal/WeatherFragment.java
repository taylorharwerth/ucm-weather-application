package com.thdf.weatherappfinal;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;


public class WeatherFragment extends Fragment {

    static TextView mLocation, mTemperature, mWindChill, mCondition, mWind, mForecast1, mForecast2, mForecast3, mForecast4, mForecast5;
    Handler handler;
    static ImageView mCondImg;
    static String mCity, mState;

    private OnFragmentInteractionListener mListener;

    public WeatherFragment() {
        handler = new Handler();
    }

    public static WeatherFragment newInstance(String param1, String param2) {
        WeatherFragment fragment = new WeatherFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_weather, container, false);
        mLocation = (TextView) rootView.findViewById(R.id.location);
        mTemperature = (TextView) rootView.findViewById(R.id.temperature);
        mWindChill = (TextView) rootView.findViewById(R.id.windchill);
        mCondition = (TextView) rootView.findViewById(R.id.condition);
        mWind = (TextView) rootView.findViewById(R.id.wind);
        mForecast1 = (TextView) rootView.findViewById(R.id.forecast1);
        mForecast2 = (TextView) rootView.findViewById(R.id.forecast2);
        mForecast3 = (TextView) rootView.findViewById(R.id.forecast3);
        mForecast4 = (TextView) rootView.findViewById(R.id.forecast4);
        mForecast5 = (TextView) rootView.findViewById(R.id.forecast5);
        mCondImg = (ImageView) rootView.findViewById(R.id.condImg);

        Context context = getActivity();
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        mCity = sharedPref.getString("city", "warrensburg");
        mState = sharedPref.getString("state", "mo");
        //mCity = "warrensburg";
        //mState = "mo";

        new RetrieveData().execute();
        return rootView;
    }

    public void changeCity(String city) {
        try {
            String CurrentString = city;
            StringTokenizer tokens = new StringTokenizer(CurrentString, ",");
            mCity = tokens.nextToken();
            mState = tokens.nextToken();
            mCity = mCity.replace(" ", "");
            mState = mState.trim();
            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("city", mCity);
            editor.commit();
            editor.putString("state", mState);
            editor.commit();
            new RetrieveData().execute();
        } catch (NoSuchElementException nse){
            Toast.makeText(getActivity(), "Invalid location. Try again!",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}

class RetrieveData extends AsyncTask<Void, Void, String> {
    public static String str;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(Void... urls) {
        try {
            URL url = new URL("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + WeatherFragment.mCity + "%2C%20" + WeatherFragment.mState + "%22)&format=xml&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                str = stringBuilder.toString();

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Error getting weather data";

    }


    protected void onPostExecute(String response) {
        if (response == null) {
            response = "Error";
        }


        // Parse xml
        try {
            int forecastCounter = 1;
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(str));
            String tagName = null;
            int event = parser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT) {
                tagName = parser.getName();
                if (event == XmlPullParser.START_TAG) {

                    if (tagName.equals("yweather:wind")) {
                        WeatherFragment.mWind.setText("Wind Speed: " + parser.getAttributeValue(null, "speed") + " mph");
                        WeatherFragment.mWindChill.setText("Wind Chill: " + parser.getAttributeValue(null, "chill"));
                    } else if (tagName.equals("yweather:location")) {
                        WeatherFragment.mLocation.setText(parser.getAttributeValue(null, "city"));
                        WeatherFragment.mLocation.append(", " + parser.getAttributeValue(null, "region"));

                    } else if (tagName.equals("yweather:condition")) {
                        WeatherFragment.mTemperature.setText("Current temperature: " + parser.getAttributeValue(null, "temp"));
                        WeatherFragment.mCondition.setText("Current conditions: " + parser.getAttributeValue(null, "text"));
                        WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                        switch (parser.getAttributeValue(null, "text")) {
                            case "Partly Cloudy":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_cloudy);
                                break;
                            case "Tornado":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_tornado);
                                break;
                            case "tropical storm":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_thunder);
                                break;
                            case "hurricane":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_thunder);
                                break;
                            case "Severe Thunderstorms":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_thunder);
                                break;
                            case "Thunderstorms":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_thunder);
                                break;
                            case "mixed rain and snow":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "mixed rain and sleet":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_drizzle);
                                break;
                            case "mixed snow and sleet":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "freezing drizzle":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_drizzle);
                                break;
                            case "Drizzle":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_drizzle);
                                break;
                            case "freezing rain":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_drizzle);
                                break;
                            case "Showers":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_drizzle);
                                break;
                            case "snow flurries":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "light snow showers":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "blowing snow":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "Snow":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "Hail":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_slightdrizzle);
                                break;
                            case "Sleet":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "Foggy":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_haze);
                                break;
                            case "Haze":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_haze);
                                break;
                            case "Windy":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_wind);
                                break;
                            case "Breezy":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_wind);
                                break;
                            case "Cold":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            case "Cloudy":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_cloudy);
                                break;
                            case "Mostly Cloudy":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_cloudy);
                                break;
                            case "Mostly Clear":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_sunny);
                                break;
                            case "Clear":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_sunny);
                                break;
                            case "Sunny":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_sunny);
                                break;
                            case "Mostly Sunny":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_sunny);
                                break;
                            case "Hot":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_sunny);
                                break;
                            case "Isolated Thunderstorms":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_thunder);
                                break;
                            case "Scattered Thunderstorms":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_thunder);
                                break;
                            case "Scattered Showers":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_thunder);
                                break;
                            case "Heavy Snow":
                                WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                                break;
                            default:
                                System.out.println("Error getting condition image...");
                        }
                        /*if (tagName.equals("yweather:code")) {
                            System.out.println("Line 152");
                            WeatherFragment.mCondImg.setImageResource(R.drawable.ic_snow);
                        }*/
                    } else if (tagName.equals("yweather:forecast")) {
                        switch (forecastCounter) {
                            case 1:
                                WeatherFragment.mForecast1.setText(parser.getAttributeValue(null, "day") + " - High: " + parser.getAttributeValue(null, "high") + " - Low: " + parser.getAttributeValue(null, "low") + " - " + parser.getAttributeValue(null, "text"));
                                forecastCounter++;
                                if (WeatherFragment.mCondition.getText().equals("28")) {
                                    //WeatherFragment.mCondImg.setImageResource(R.drawable.ic_sunny);//This works
                                }
                                break;
                            case 2:
                                WeatherFragment.mForecast2.setText(parser.getAttributeValue(null, "day") + " - High: " + parser.getAttributeValue(null, "high") + " - Low: " + parser.getAttributeValue(null, "low") + " - " + parser.getAttributeValue(null, "text"));
                                forecastCounter++;
                                break;
                            case 3:
                                WeatherFragment.mForecast3.setText(parser.getAttributeValue(null, "day") + " - High: " + parser.getAttributeValue(null, "high") + " - Low: " + parser.getAttributeValue(null, "low") + " - " + parser.getAttributeValue(null, "text"));
                                forecastCounter++;
                                break;
                            case 4:
                                WeatherFragment.mForecast4.setText(parser.getAttributeValue(null, "day") + " - High: " + parser.getAttributeValue(null, "high") + " - Low: " + parser.getAttributeValue(null, "low") + " - " + parser.getAttributeValue(null, "text"));
                                forecastCounter++;
                                break;
                            case 5:
                                WeatherFragment.mForecast5.setText(parser.getAttributeValue(null, "day") + " - High: " + parser.getAttributeValue(null, "high") + " - Low: " + parser.getAttributeValue(null, "low") + " - " + parser.getAttributeValue(null, "text"));
                                forecastCounter++;
                                break;
                        }

                    }
                }
                event = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //mWeatherData.setText(str);

    }

}